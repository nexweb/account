package com.demo.microservices.account.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AccountTransfer {
	private String srcAccNo;
	private String targetAccNo;
	private String userNm;
	private String bank;
	private Long amount;
}

